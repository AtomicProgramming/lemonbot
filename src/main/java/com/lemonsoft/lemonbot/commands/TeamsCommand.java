package com.lemonsoft.lemonbot.commands;

import com.lemonsoft.lemonbot.DiscordCommandHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static java.lang.System.nanoTime;

public class TeamsCommand implements DiscordCommandHandler {
    private static int[] generateRandomRange(int size) {
        var linearRange = IntStream.range(0, size).toArray();
        var rng = ThreadLocalRandom.current();
        for (int i = 0; i < size; i++) {
            int swapIndex = rng.nextInt(size);
            int upIndex = linearRange[swapIndex];
            linearRange[swapIndex] = linearRange[i];
            linearRange[i] = upIndex;
        }
        return linearRange;
    }

    public void handle(Message message) {
        var before = nanoTime();
        List<Member> players;
        int teamCount;

        var args = message.getContentRaw().split(" ");
        if (args.length < 2) {
            message.getChannel().sendMessage("**Error!** Too few arguments. See `help`").queue();
            return;
        }
        try {
            teamCount = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            message.getChannel().sendMessage(String.format("**Error!** '%s' is not a valid number.", (args[1]))).queue();
            return;
        }
        if (teamCount < 2 || teamCount > 9) {
            message.getChannel().sendMessage("**Error!** Pick a team count between [2-9].").queue();
            return;
        }

        if (args.length == 2) {
            // get players from voicechat
            var voiceChannel = message.getMember().getVoiceState().getChannel();
            if (voiceChannel == null) {
                message.getChannel().sendMessage("**Error!** No users specified. Join a voice channel or specify players.").queue();
                return;
            } else {
                players = new ArrayList<>(voiceChannel.getMembers());
            }
        } else {
            // get players from mentions
            players = new ArrayList<>(message.getMentionedMembers());
            if (players.isEmpty()) {
                message.getChannel().sendMessage("**Error!** Invalid mention.").queue();
                return;
            }
        }
        Collections.shuffle(players);

        // how many times we can add one user to every team
        var fullTeams = players.size() / teamCount;
        // how many users are left without a team (remainder)
        var remainder = players.size() % teamCount;

        var mentionsIterator = players.iterator();
        var teams = new ArrayList<ArrayList<Member>>();
        for (int i = 0; i < teamCount; i++) {
            var newTeam = new ArrayList<Member>();
            for (int j = 0; j < fullTeams; j++) {
                newTeam.add(mentionsIterator.next());
            }
            teams.add(newTeam);
        }

        if (remainder > 0) {
            var randomTeamRange = generateRandomRange(teamCount);
            for (int i = 0; i < remainder; i++) {
                // get a random team and add random remainder user
                teams.get(randomTeamRange[i]).add(mentionsIterator.next());
            }
        }

        var embed = new EmbedBuilder()
                .setColor(new Color(144, 224, 110))
                .setTitle("Done!");

        for (int i = 0; i < teams.size(); i++) {
            var renderedArray = new StringBuilder();
            for (var player : teams.get(i)) {
                renderedArray.append(String.format("\u2022 %s\n", player.getUser().getAsTag()));
            }
            embed.addField(String.format("Team %d", i+1), renderedArray.toString(), true);
        }
        embed.setFooter(String.format("Done in %d \u00B5s", ((nanoTime()-before)/1000)));
        message.getChannel().sendMessage(embed.build()).queue();
    }
}
