package com.lemonsoft.lemonbot.commands;

import com.lemonsoft.lemonbot.OwnerCommandHandler;
import net.dv8tion.jda.api.entities.Message;

public class KillCommand extends OwnerCommandHandler {
    protected void handleOwner(Message message) {
        var messageText = message.getContentRaw();
        var spaceIndex = messageText.indexOf(' ');
        if (spaceIndex == -1) {
            message.getChannel().sendMessage("**Error!** No PID specified.").queue();
            return;
        }
        long pid;
        try {
            pid = Long.parseLong(messageText.substring(spaceIndex+1));
        } catch (NumberFormatException e) {
            message.getChannel().sendMessage("**Error!** Specified PID is incorrect.").queue();
            return;
        }
        var process = ProcessHandle.allProcesses().filter(p -> p.pid() == pid).findFirst();
        if (process.isPresent()) {
            message.getChannel().sendMessage("**Done!** A kill signal has been sent.").queue();
            process.get().destroy();
        } else {
            message.getChannel().sendMessage("**Error!** Specified PID doesn't correspond to any process.").queue();
        }
    }
}
