package com.lemonsoft.lemonbot.commands;

import com.lemonsoft.lemonbot.LemonBot;
import com.lemonsoft.lemonbot.OwnerCommandHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.OperatingSystem;

import java.awt.*;
import java.util.concurrent.TimeUnit;

public class SystemInfoCommand extends OwnerCommandHandler {
    OperatingSystem os;
    HardwareAbstractionLayer hal;

    public SystemInfoCommand() {
        var systemInfo = new SystemInfo();
        os = systemInfo.getOperatingSystem();
        hal = systemInfo.getHardware();
    }

    protected void handleOwner(Message message) {
        var embed = new EmbedBuilder().setColor(new Color(232, 35, 101)).setTitle("System Info");
        var totalRam = hal.getMemory().getTotal();
        var freeRam = hal.getMemory().getAvailable();
        var cpu = hal.getProcessor();
        var load = cpu.getSystemLoadAverage(3);
        var board = hal.getComputerSystem().getBaseboard();
        var millis = System.currentTimeMillis() - LemonBot.startTime;
        var days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        var hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        var mins = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(mins);
        var secs = TimeUnit.MILLISECONDS.toSeconds(millis);
        embed.setDescription(String.format(
                "**Hardware**\n__CPU__: %dx %s\n__Board__: %s %s %s\n\n**Software**\n__OS__: %s %s [%d-bit]\n__Memory__: %dMB / %dMB used\n__Load average__: %.2f | %.2f | %.2f\n\n**Bot uptime:** %d days, %d hours, %d minutes, %d seconds",
                cpu.getLogicalProcessorCount(), cpu.getProcessorIdentifier().getName(), // CPU
                board.getManufacturer(), board.getModel(), board.getVersion(), // Board
                os.getFamily(), os.getVersionInfo(), os.getBitness(), // OS
                (totalRam-freeRam)/1048576, totalRam/1048576, // Memory
                load[0], load[1], load[2], // Load average
                days, hours, mins, secs // Bot uptime
        ));
        message.getChannel().sendMessage(embed.build()).queue();
    }
}
