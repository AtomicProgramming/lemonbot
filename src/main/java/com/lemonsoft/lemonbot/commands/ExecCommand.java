package com.lemonsoft.lemonbot.commands;

import com.lemonsoft.lemonbot.OwnerCommandHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExecCommand extends OwnerCommandHandler {
    private static final Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");

    private static List<String> parseCommandWithArgs(String commandline) {
        List<String> matchList = new ArrayList<>();
        Matcher regexMatcher = regex.matcher(commandline);
        while (regexMatcher.find()) {
            if (regexMatcher.group(1) != null) {
                matchList.add(regexMatcher.group(1));
            } else if (regexMatcher.group(2) != null) {
                matchList.add(regexMatcher.group(2));
            } else {
                matchList.add(regexMatcher.group());
            }
        }
        return matchList;
    }

    private static List<String> splitIntoCodeblocks(String text) {
        List<String> ret = new ArrayList<>((text.length() + 1990 - 1) / 1990);

        for (int start = 0; start < text.length(); start += 1990) {
            ret.add(String.format("```\n%s```", text.substring(start, Math.min(text.length(), start + 1990))));
        }
        return ret;
    }

    protected void handleOwner(Message message) {
        var messageText = message.getContentRaw();
        var spaceIndex = messageText.indexOf(' ');
        if (spaceIndex == -1) {
            message.getChannel().sendMessage("**Error!** No command specified.").queue();
            return;
        }
        var builder = new StringBuilder();
        Process process;
        String command = messageText.substring(spaceIndex+1);
        try {
            process = new ProcessBuilder(parseCommandWithArgs(command)).start();
        } catch (IOException e) {
            message.getChannel().sendMessage(String.format("**Error!** Couldn't start the process.\n```\n%s```", e.getMessage())).queue();
            return;
        }
        var startInstant = process.info().startInstant().orElse(Instant.now());
        var embed = new EmbedBuilder().setColor(new Color(119, 124, 201)).setTitle("Process launched")
                .addField("Details", String.format("PID: `%d`\nUser: `%s`\nCommandline: `%s`", process.pid(),
                        process.info().user().orElse("n/a"), command), false);
        message.getChannel().sendMessage(embed.build()).queue();
        var reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        var errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        new Thread(() -> {
            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                    builder.append('\n');
                }
                while ((line = errorReader.readLine()) != null) {
                    builder.append(line);
                    builder.append('\n');
                }
                process.waitFor();
            } catch (IOException | InterruptedException e) {
                message.getChannel().sendMessage("**Error!** Couldn't read the standard input.").queue();
                return;
            }
            embed.setColor(new Color(119, 201, 129)).setTitle("Process completed");
            var output = builder.toString();
            embed.addField("Result", String.format("__Time__: %d ms\n__Exit code__: %d",
                    Duration.between(startInstant, Instant.now()).toMillis(), process.exitValue()), false);
            if (output.length() <= 1010) {
                embed.addField("Output", String.format(
                        "```\n%s```", output.isEmpty() ? "The process exited with no output." : output
                ), false);
            } else {
                embed.setFooter("The output exceeds 1000 characters and has been sent in separate embeds.");
                for (var codeblock : splitIntoCodeblocks(output)) {
                    message.getChannel().sendMessage(codeblock).complete();
                }
            }
            message.getChannel().sendMessage(embed.build()).queue();
        }).start();
    }
}
