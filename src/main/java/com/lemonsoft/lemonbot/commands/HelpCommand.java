package com.lemonsoft.lemonbot.commands;

import com.lemonsoft.lemonbot.CommandListener;
import com.lemonsoft.lemonbot.DiscordCommandHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HelpCommand implements DiscordCommandHandler {
    private final Map<String, List<HelpEntry>> helpEntries = new HashMap<>();
    private MessageEmbed helpEmbed;

    private static class HelpEntry {
        final String command;
        final String description;
        final String usage;

        public HelpEntry(String command, String description, String usage) {
            this.command = command;
            this.description = description;
            this.usage = usage;
        }
    }

    public void addHelpEntry(String command, String commandDescription, String commandUsage, String commandGroup) {
        helpEntries.computeIfAbsent(commandGroup, k -> new ArrayList<>()).add(new HelpEntry(command, commandDescription, commandUsage));
    }

    public void addHelpEntry(String command, String commandDescription, String commandUsage) {
        addHelpEntry(command, commandDescription, commandUsage, "Misc");
    }

    public void renderHelp() {
        var embed = new EmbedBuilder().setTitle("LemonBot Commands").setColor(new Color(121, 237, 198));
        for (Map.Entry<String, List<HelpEntry>> group : helpEntries.entrySet()) {
            var currentGroupHelp = new StringBuilder();
            for (HelpEntry helpEntry : group.getValue()) {
                currentGroupHelp.append(String.format("\n*%s%s*\n%s\nUsage: `%s%s`\n", CommandListener.PREFIX, helpEntry.command, helpEntry.description, CommandListener.PREFIX, helpEntry.usage));
            }
            embed.addField(String.format("  \u2022 __**%s commands**__", group.getKey()), currentGroupHelp.toString(), true);
        }
        helpEmbed = embed.build();
    }

    public void handle(Message message) {
        message.getChannel().sendMessage(helpEmbed).queue();
    }
}
