package com.lemonsoft.lemonbot.commands;

import com.lemonsoft.lemonbot.DiscordCommandHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.awt.*;

public class EvalCommand implements DiscordCommandHandler {
    public void handle(Message message) {
        var messageText = message.getContentRaw();
        var spaceIndex = messageText.indexOf(' ');
        if (spaceIndex == -1) {
            message.getChannel().sendMessage("**Error!** No expression specified.").queue();
            return;
        }
        var expression = messageText.substring(spaceIndex+1);
        String expressionResult;
        try {
            expressionResult = String.valueOf(new ExpressionBuilder(expression).build().evaluate());
        } catch (Exception e) {
            message.getChannel().sendMessage(
                    new EmbedBuilder().setTitle("Math error").setDescription(e.getMessage()).setColor(new Color(255, 135, 105)).build()
            ).queue();
            return;
        }
        message.getChannel().sendMessage(
                new EmbedBuilder()
                        .setTitle("Math result").setColor(new Color(172, 219, 101))
                        .setDescription(expressionResult)
                        .build())
                .queue();
    }
}
