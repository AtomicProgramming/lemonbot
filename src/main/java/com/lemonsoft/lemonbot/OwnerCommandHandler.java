package com.lemonsoft.lemonbot;

import net.dv8tion.jda.api.entities.Message;

public abstract class OwnerCommandHandler implements DiscordCommandHandler {
    static final long OWNER_ID = 339413896903262209L;
    protected abstract void handleOwner(Message message);

    public void handle(Message message) {
        if (message.getAuthor().getIdLong() == OWNER_ID) {
            handleOwner(message);
        } else {
            message.getChannel().sendMessage("**Error!** Insufficient permissions.").queue();
        }
    }
}
