package com.lemonsoft.lemonbot;

import net.dv8tion.jda.api.entities.Message;

public interface DiscordCommandHandler {
    void handle(Message message);
}
