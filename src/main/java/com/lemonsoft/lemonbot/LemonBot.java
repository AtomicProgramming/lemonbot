package com.lemonsoft.lemonbot;

import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class LemonBot {
    public static final long startTime = System.currentTimeMillis();

    public static void main(String[] argv) throws LoginException {
        // get token from dtoken.txt
        String token;
        try {
            token = Files.readString(Path.of("dtoken.txt"));
        } catch (IOException e) {
            System.err.println("ERROR: Couldn't open 'dtoken.txt'.");
            System.exit(1); return;
        }

        var jda = JDABuilder.create(token, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_VOICE_STATES, GatewayIntent.GUILD_MESSAGE_REACTIONS)
                .addEventListeners(new CommandListener())
                .setActivity(Activity.watching("you"))
                .disableCache(CacheFlag.ACTIVITY, CacheFlag.EMOTE, CacheFlag.CLIENT_STATUS)
                .build();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Exiting...");
            jda.shutdown();
        }));
    }
}
