package com.lemonsoft.lemonbot;

import com.lemonsoft.lemonbot.commands.*;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CommandListener extends ListenerAdapter {
    public static final String PREFIX = "w!";

    private final Logger log = LoggerFactory.getLogger(CommandListener.class);
    private final Map<String, DiscordCommandHandler> handlers = new HashMap<>();
    private static final int PREFIX_LENGTH = PREFIX.length();
    private static long selfUserId;

    public void addHandler(String command, DiscordCommandHandler handler) {
        handlers.put(command, handler);
    }

    @Override
    public void onReady(@NotNull ReadyEvent event) {
        log.info(String.format("Logged in as %s", event.getJDA().getSelfUser().getAsTag()));
        selfUserId = event.getJDA().getSelfUser().getIdLong();
        // add command handlers
        var teamsCommand = new TeamsCommand();
        addHandler("teams", teamsCommand);
        addHandler("team", teamsCommand);
        addHandler("exec", new ExecCommand());
        addHandler("sysinfo", new SystemInfoCommand());
        addHandler("math", new EvalCommand());
        addHandler("kill", new KillCommand());

        // add help
        var helpCommand = new HelpCommand();
        helpCommand.addHelpEntry("sysinfo", "Get system information.", "sysinfo", "Owner-only");
        helpCommand.addHelpEntry("exec", "Execute a system command.", "exec <commandline>", "Owner-only");
        helpCommand.addHelpEntry("kill", "Kill a process by its PID.", "kill <PID>", "Owner-only");
        helpCommand.addHelpEntry("teams", "Split players into teams. If you omit mentions, your current voice channel will be used as the player list.", "teams <number of teams> [mentions]", "Utility");
        helpCommand.addHelpEntry("math", "Evaluate a math expression.", "math <expression>", "Utility");
        helpCommand.renderHelp();
        addHandler("help", helpCommand);
        log.info(String.format("Loading done! Press Ctrl-C to exit. [%dms]", System.currentTimeMillis() - LemonBot.startTime));
    }

    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
        try {
            if (event.getAuthor().getIdLong() == selfUserId) return; // don't process our own messages
            var contentRaw = event.getMessage().getContentRaw();
            if (!contentRaw.startsWith(PREFIX)) {
                return;
            }

            // determine where is the command in the string
            // for example: w!teams 4 @user @user
            //                ^^^^^  <- we want this
            String command;
            var commandEnd = contentRaw.indexOf(' ');
            if (commandEnd == -1) {
                command = contentRaw.substring(PREFIX_LENGTH);
            } else {
                command = contentRaw.substring(PREFIX_LENGTH, commandEnd);
            }

            // get the handler for a given command
            var handler = handlers.get(command);
            if (handler == null) {
                event.getChannel().sendMessage("**Error!** Unknown command.").queue();
            } else {
                handler.handle(event.getMessage());
            }
        } catch (Exception e) {
            event.getChannel().sendMessage("**Error!** An exception has occurred.").queue();
            e.printStackTrace();
        }
    }
}
